<?php
    if (isset($_POST["log_username"])) {
        $credentials = $_POST["log_username"] . ',' . $_POST["log_password"];
        $cache = fopen("cache","r");
        $tmp = fread($cache,filesize('cache'));
        $db = explode(";", $tmp);
        if (is_numeric(array_search($credentials, $db))) {
            ?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="resources/style.css">
                <title>Bravo !</title>
            </head>
            <body>
            <div class="fullscreen-bg">
                <video loop controls autoplay class="fullscreen-bg__video">
                    <source src="resources/bravo.mp4" type="video/mp4">
                </video>
            </div>
            <a href="https://www.youtube.com/watch?v=8Ib8BXg8_qU">
                <button class="secret_button ">Detruire Internet</button>
            </a>
            </body>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            </html>
            <?php
        }
        else  {
            echo '<script>window.location.replace("http://localhost:4000/home.php?wrong_credentials=true");</script>';
        }
        fclose($cache);
        $_POST["log_username"] = null;
        $_POST["log_password"] = null;
    } else {
        echo '<script>window.location.replace("http://localhost:4000/home.php?not_logged=true");</script>';
    } 
?>
