<?php 
    unset($_POST["log_username"]);
    unset($_POST["log_password"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="resources/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Task 5 M6</title>
</head>
<body>
    <div class="container">
        <?php 
            if (isset($_GET["not_logged"])) {
                echo '<div class="alert alert-danger" role="alert">Veuillez vous connecter avant d acceder a cette page</div>';
            }
            if (isset($_GET["wrong_credentials"])) {
                echo '<div class="alert alert-danger" role="alert">L identification a échoué</div>';
            }
            if (isset($_GET["duplicate"])) {
                echo '<div class="alert alert-danger" role="alert">Le nom d utilisateur spécifié existe déjà</div>';
            }
        ?>
        <div class="login">
            <h1 class="box_title">LOGIN</h1>
            <form action="http://localhost:4000/secret.php" method="POST">
                <input class="box_input" type="text" name="log_username" placeholder="Identifiant">
                <br>
                <input class="box_input" type="password" name="log_password" placeholder="Mot de passe">
                <br>
                <input class="btn btn-primary" type="submit" value="Connexion" method="POST">
            </form>
        </div>
        <div class="register">
            <h1 class="box_title">REGISTER</h1>
            <form action="home.php" method="post">
                <input class="box_input" type="text" name="reg_username" placeholder="Identifiant">
                <br>
                <input class="box_input" type="password" name="reg_password" placeholder="Mot de passe">
                <br>
                <input class="box_input" type="password" name="reg_confirm_password" placeholder="Confirmez mot de passe">
                <br>
                <input class="btn btn-primary" type="submit" value="Enregistrer">
            </form>
            <?php
                if (isset($_POST["reg_username"])) {
                    $name = $_POST["reg_username"];
                    $pass = $_POST["reg_password"];
                    $conf_pass = $_POST["reg_confirm_password"];
                    if ($pass == $conf_pass) {
                        if (filesize('cache') > 0) {
                            $credentials = $name . ',' . $pass;
                            $cache = fopen("cache","r");
                            $tmp = fread($cache,filesize('cache'));
                            $db = explode(";", $tmp);
                            foreach($db as $pair) {
                                $check_name = explode(",", $pair)[0];
                                if ($name == $check_name) {
                                    echo '<script>window.location.replace("http://localhost:4000/home.php?duplicate=true");</script>';
                                }
                            }
                            if (is_numeric(array_search($credentials, $db))) {
                                echo '<div class="alert alert-danger" role="alert">L identifiant spécifé existe déjà</div>';
                            }
                            else {
                                $cache = fopen("cache","a");
                                fwrite($cache, "$name,$pass;");
                                fclose($cache);
                                echo '<div class="alert alert-success" role="alert">Votre compte a bien été créé</div>';
                            }
                            
                        }
                        else {
                            $cache = fopen("cache","a");
                            fwrite($cache, "$name,$pass;");
                            fclose($cache);
                            echo '<div class="alert alert-success" role="alert">Votre compte a bien été créé</div>';
                        }
                    }
                    else  {
                        echo '<div class="alert alert-danger" role="alert">Les deux mots de passe ne correspondent pas.</div>';
                    }
                    unset($_POST["log_username"]);
                    unset($_POST["log_password"]);
                }
            ?>
        </div>
    </div>
</body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>